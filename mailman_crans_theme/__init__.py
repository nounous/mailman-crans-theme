# Copyright (C) 2021 Crans
# SPDX-License-Identifier: GPL-3.0

"""
Mailman3 Crans theme.
"""

try:
    from mailman_crans_theme.version import version as __version__
except ImportError:
    __version__ = "dev"

# See https://www.python.org/dev/peps/pep-0008/#module-level-dunder-names
__all__ = [
    "__version__",
]
