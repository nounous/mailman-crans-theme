# Mailman3 Crans theme

Crans visual identity for Mailman3 Hyperkitty and Postorius.

```bash
sudo pip install --upgrade git+https://gitlab.crans.org/nounous/mailman-crans-theme.git
sudo mailman-web collectstatic
sudo systemctl restart mailman3-web.service
```
